'use strict';

module.exports = function() {
	$.gulp.task('genimage', function () {
		var spriteData = $.gulp.src('source/images/sprites/*.*').pipe($.gp.spritesmith({
			imgName: 'img/sprite.png',
			cssName: 'css/sprite.css'
		}));
		return spriteData.pipe($.gulp.dest('build/assets/'));
	});
};