import menu from "./modules/menu.js";
import {runGallery, prepareGallery} from "./modules/gallery.js";
import cart from "./modules/cart.js";
import compare from "./modules/compare.js";

$(document).ready(function () {
	var swiper = new Swiper('.swiper-container', {
		loop: true,
		pagination: {
			el: '.slider__count',
			type: 'fraction',
		},
		navigation: {
			nextEl: '.slider__front',
			prevEl: '.slider__back',
		},
	});
	menu();
	runGallery();
	cart();
	compare();
});

$(window).resize(function () {
	menu();
	prepareGallery();
});