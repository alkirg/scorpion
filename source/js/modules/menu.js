const nav = $('.mainmenu');
const menu = nav.children('.mainmenu__items');
const menuItem = $('.mainmenu__item'); 
const menuLink = $('.mainmenu__link');
let mobileShown = false;
let keepShadow = false;

const run = () => {
	if ( ! isMobile()) {
		menuLink.on('mouseenter', function () {
			$(this).next().addClass('mainmenu__items--active');
			showShadow();
			keepShadow = true;
			$('.mainmenu__items--active').mouseenter(function () {
				$(this).addClass('mainmenu__items--active');
				showShadow();
				keepShadow = true;
			});
			$('.mainmenu__items--active').mouseleave(function (e) {
				$(this).removeClass('mainmenu__items--active');
				if (!(e.pageY > $('.mainmenu').position().top && e.pageY <= $('.mainmenu').position().top + $('.mainmenu').height())) {
					keepShadow = false;
					removeShadow();
				}
			});
		});
		menuLink.on('mouseleave', function () {
			$(this).next().removeClass('mainmenu__items--active');
		});
		$('.mainmenu__items').on('mouseleave', function (e) {
			if (!(e.pageY > $('.mainmenu').position().top && e.pageY <= $('.mainmenu').position().top + $('.mainmenu').height())) {
				keepShadow = false;
				removeShadow();
			}
		});
		nav.on('mouseleave', function (e) {
			keepShadow = false;
			removeShadow();
		});
	} else {
		let navMobile = nav;
		navMobile.click(function() {
			if ( ! mobileShown) {
				navMobile.prepend('<div class="icon_close"></div>');
				navMobile.addClass('mobilemenu').animate({left: '0px'}, 200);
				$('body').prepend(navMobile);
				$('.search').parent().prepend('<div class="mainmenu"></div>');
				$('.header, .main, .footer').wrapAll('<div class="mobilemenu__shadow"></div>');
				$('.mobilemenu__shadow').animate({left: '300px'}, 200);
				$('.search').css('margin-left', 'auto');
				$('.mainmenu__parent').removeClass('mainmenu__parent');
				$('.mobilemenu .icon_close, .mobilemenu__shadow').click(function() {
					navMobile.animate({left: '-300px'}, 200, function() {
						$(this).removeClass('mobilemenu');
					});
					$('.mobilemenu__shadow').animate({left: '0px'}, 200, function () {
						$('.header, .main, .footer').unwrap();
						$('.header__row--yellow .header__row .mainmenu').remove();
						$('.header__row--yellow .header__row').prepend(nav);
						$('.mainmenu .icon_close').remove();
						$('.search').css('margin-left', '');
						mobileShown = false;
					});
				});
				mobileShown = true;
			}
		});
	}
};

const showShadow = () => {
	if (!keepShadow && !isMobile()) {
		$('body').prepend('<div class="shadow"></div>');
		let shadow = $('.shadow');
		shadow.css('opacity', 0);
		shadow.animate({opacity: '.5'}, 100);
	}
};

const removeShadow = () => {
	if (!keepShadow) {
		let shadow = $('.shadow');
		shadow.animate({opacity: '0'}, 100, function () {
			shadow.remove();
		});
	}
};

const isMobile = () => {
	return ($('.main').width() <= 970);
};

export default run;