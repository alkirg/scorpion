const gallery = $('.gallery');
let thumbs = gallery.find('.gallery__thumb');

const runGallery = () => {
	prepareGallery();
	thumbs.click(function (e) {
		const thumb = $(e.target);
		const imageCurrent = $('.gallery__main');
		if (imageCurrent.attr('src') !== thumb.attr('data-img')) {
			thumbs.removeClass('gallery__thumb--active');
			imageCurrent.attr('src', thumb.attr('data-img'));
			thumb.addClass('gallery__thumb--active');
		}
	});
};

const prepareGallery = () => {
	thumbs.first().addClass('gallery__thumb--active');
};

export {runGallery, prepareGallery};