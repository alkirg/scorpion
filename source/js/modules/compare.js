const run = () => {
	$('.compare__item .icon_close').click(function () {
		let that = $(this);
		let item = that.parent();
		item.fadeOut(400, function () {
			item.remove();
			if ($('.compare__item').length == 0)
				$('.compare').html('<p class="error">You have nothing to compare.</p>');
		});
	});
};

export default run;