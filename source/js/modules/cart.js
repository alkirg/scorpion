const run = () => {
	$('.cart__button--less').click(function () {
		let that = $(this);
		if (!that.hasClass('cart__button--disabled')) {
			let input = that.next();
			let newVal = parseInt(input.val()) - 1;
			input.val(newVal);
			if (newVal == 1)
				that.addClass('cart__button--disabled');
			input.change();
		}
	});
	
	$('.cart__button--more').click(function () {
		let that = $(this);
		let input = that.prev();
		let newVal = parseInt(input.val()) + 1;
		input.val(newVal);
		if (newVal == 2)
			input.prev().removeClass('cart__button--disabled');
		input.change();
	});
	
	$('.cart__count').change(function () {
		let that = $(this);
		let value = parseInt(that.val());
		if (value) {
			that.val(value);
			if (value == 1)
				that.prev().addClass('cart__button--disabled');
		}
		else {
			that.val(1);
			that.prev().addClass('cart__button--disabled');
		}
		
		countCart();
	});
	
	$('.cart__remove a').click(function (e) {
		e.preventDefault();
		
		let that = $(this); 
		$.get(that.attr('href'), function (data) {
			if (data.delete) {
				let tr = that.parents('tr');
				tr.fadeOut(400, function () {
					tr.remove();
					if ($('.cart__table tr').length > 1)
						countCart();
					else {
						$('.main .cart').html('<p class="error">Your cart is empty.</p>');
					}
				});
			}
		});
	});
};

const countCart = () => {
	let cartTotal = 0;
	
	$('.cart__item_price').each(function () {
		let that = $(this);
		let count = parseInt(that.next().find('.cart__count').val());
		let total = parseFloat(that.children('strong').children('span').html()) * count;
		cartTotal += parseFloat(total.toFixed(2));
		that.siblings('.cart__item_total').children('strong').children('span').html(formatPrice(total.toFixed(2)));
	});
	
	$('.cart__total_price strong span').html(formatPrice(cartTotal.toFixed(2)));
};

const formatPrice = (value) => {
	return Intl.NumberFormat('en-US', {'minimumFractionDigits': 2}).format(value).replace(',', ' ');
};

export default run;